const puppeteer = require('puppeteer')

exports.printPDF = async function printPDF() {
    const browser = await puppeteer.launch({
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
        ],
    });
    const page = await browser.newPage();
    await page.goto('https://serpicodev-invoice.herokuapp.com/pdf', { waitUntil: 'networkidle0' });
    // await page.goto('http://localhost:3000/pdf', { waitUntil: 'networkidle0' }); // Local Test

    const pdf = await page.pdf({ format: 'A4' });

    await browser.close();
    return pdf
};