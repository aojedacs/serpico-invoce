require('dotenv').config();
var express = require('express');
var router = express.Router();
const request = require('request-promise');
var bcrypt = require('bcryptjs');
var moment = require('moment');
var ls = require('local-storage');
var userdb = require('../models/user');
var pdf = require('html-pdf');
var pdf = require('./pdf');
const url = require('url');



/* GET home page. */

// dates = {
//     start: moment().startOf('month').subtract(1, 'month').format("YYYY-MM-DD[T]HH:mm:ss"),
//     end: moment().subtract(1, 'month').endOf('month').format("YYYY-MM-DD[T]HH:mm:ss")
// }


// res.json(dates)



router.get('/', function(req, res, next) {
    var messageData = {
        style: 'display:none; color:#ff0000',
        message: 'Incorrect password'
    }
    res.render('index', messageData);
});

router.get('/invoice', function(req, res, next) {

    pdf.printPDF().then(pdf => {
        res.set({ 'Content-Type': 'application/pdf', 'Content-Length': pdf.length })
        res.send(pdf)
    });

    // var userdata = ls.get('contactInfoAndtimes');

    // console.log('USERDATA', JSON.stringify(userdata))

    // res.render('invoice', userdata);
});

router.get('/pdf', function(req, res, next) {

    var userdata = ls.get('contactInfoAndtimes');

    console.log('USERDATA', JSON.stringify(userdata))

    res.render('invoice', userdata);
});

// router.get('/test', function(req, res, next) {

//     var messageData = {
//         style: 'display:none; color:#ff0000',
//         message: 'Incorrect password'
//     }

//     var user = {
//         email: 'angel@serpicodev.com',
//         pass: '1234567890',
//         contactInfo: {
//             id: '00',
//             firstName: 'a',
//             lastName: 'asdfdfsdf',
//             userInfo: 'asdfasdfasdf',
//             paymentInfo: 'asdfasdfa',
//             paymentRate: 10
//         }
//     }

//     userdb.getByEmail('angel.ojeda@serpicodev.com').then(foundUser => {
//         console.log('OLD', foundUser)
//         userdb.update(user).then(r => {
//             console.log('RESPUESTA', r)
//         }).catch(e => console.log("Error has ocurred ", e));
//     }).catch(e => console.log("Error has ocurred ", e));


//     res.render('index', messageData);
// });

router.get('/invoicetest', function(req, res, next) {

    var userdata = { "id": "KUAGA2M5", "firstName": "Angel", "lastName": "Ojeda", "userInfo": "City: La Paz B.C.S, Mexico\r\nAddress: Peru # 296\r\nZip code: 23085\r\n", "paymentInfo": "Beneficiary Bank Information\r\nBank Name: BBVA BANCOMER\r\nSwift code : BCMRMXMMPYM\r\nBeneficiary Account Information\r\nAccount Owner Name : ANGEL DE JESUS OJEDA CASTRO\t\t\r\nAccount Owner Address : PERU #296 FRACC. LAS AMERICAS\r\nAccount Number (CLABE / IBAN) : 012040015868363728\r\n", "paymentRate": "15", "times": [{ "projectId": "IEACXBAZI4LLETKL", "tasks": [{ "id": "IEACXBAZKQLKVYUU", "title": "TESTING SUBTASK", "total": 0.33, "times": [{ "description": "TESTING CREATION", "time": 0.33 }] }, { "id": "IEACXBAZKQLFXDK3", "title": "Update webpage for July meeting", "total": 1.5, "times": [{ "description": "- Added carousel with quotes from customers.\n- Deployment changes in website.", "time": 1.5 }] }], "time": 1.83, "title": "AZFG 03 Tasks" }, { "projectId": "IEACXBAZI7777777", "tasks": [{ "id": "IEACXBAZKQLAG3SL", "title": "WMIG Archive old projects", "total": 11, "times": [{ "description": "- Restructuration for the project to save everithing in a database first and then export all data into a document", "time": 5 }, { "description": "- Coding project to export ryver data and shown it into a webpage for login so user can login and export individually all data for each section into a xls document.\n- Conection and deployment for the project.", "time": 6 }] }, { "id": "IEACXBAZKQLFYG66", "title": "WMIG invoice", "total": 1, "times": [{ "description": "- Research about API and testing to view functionality.", "time": 1 }] }], "time": 12, "title": "NA" }] }
    res.render('invoice', userdata);
});

router.post('/change', function(req, res, next) {

    var oldPass = req.body.oldPass;
    var newPass = req.body.newPass;
    var confNewPass = req.body.confNewPass;
    var salt = bcrypt.genSaltSync(10);
    var newPassHash = bcrypt.hashSync(newPass, salt);
    var email = ls.get('email');

    console.log('MAIL IS', ls.get('email'))


    if (bcrypt.compareSync(oldPass, ls.get('defPass'))) {

        console.log('LASWESA ', newPass.localeCompare(confNewPass))

        if (newPass.localeCompare(confNewPass) == 0) {
            userdb.getByEmail(email).then(foundUser => {
                console.log('OLD', foundUser)
                foundUser.pass = newPassHash
                console.log('NEW', foundUser)

                userdb.delete(email).then(r => {
                    // res.redirect(`https://www.wrike.com/oauth2/authorize/v4?client_id=${process.env.CLIENT_ID}&response_type=code`);
                    userdb.create(foundUser).then(r => {
                        ls('email', email.toLowerCase().trim());
                        res.redirect(`https://www.wrike.com/oauth2/authorize/v4?client_id=${process.env.CLIENT_ID}&response_type=code`);

                    }).catch(e => { //if someone doesnt change password or a reset was made
                        if (e.code == 400) {
                            console.log('ERROR ', e)
                            var messageData = {
                                style: 'display:block; color:#ff0000',
                                message: 'Error creating user'
                            }
                            res.render('index', messageData);
                        }
                    });


                }).catch(e => console.log("Error has ocurred ", e));
                // userdb.update(foundUser).then(r => {
                //     res.redirect(`https://www.wrike.com/oauth2/authorize/v4?client_id=${process.env.CLIENT_ID}&response_type=code`);
                // }).catch(e => console.log("Error has ocurred ", e));
            }).catch(e => console.log("Error has ocurred ", e));
        } else {
            res.redirect(url.format({
                pathname: "/changepass",
                query: {
                    style: 'display:block; color:#ff0000',
                    message: 'New password and confirm doesn\'t match'
                }
            }))
        }
    } else {
        res.redirect(url.format({
            pathname: "/changepass",
            query: {
                style: 'display:block; color:#ff0000',
                message: 'Old pass doesn\'t match'
            }
        }))
    }
});

router.get('/changepass', function(req, res, next) {

    console.log('laqueri', req.query.style)

    var data = {
        style: req.query.style,
        message: req.query.message
    }

    res.render('changepass', data)
});

router.get('/administrator', function(req, res, next) {
    userdb.getAll().then(users => {

        users.map(user => {
            console.log("Mail ", user.email)
            console.log("First name ", user.contactInfo.firstName)
            console.log("First name ", user.contactInfo.lastName)

        })
        res.render('administrator', { data: users });

    }).catch(e => console.log("Error has ocurred ", e));
});

router.post('/auth', function(req, res, next) {

    const email = req.body.email.toLowerCase().trim();
    const tempPass = '12345678';
    const adminEmail = 'admin@serpicodev.com'
    const pass = req.body.pass;
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(pass, salt);
    var tempHash = bcrypt.hashSync(tempPass, salt);
    ls('defPass', tempHash);


    var user = {
        email: email.toLowerCase().trim(),
        pass: tempHash,
        contactInfo: {
            id: '',
            firstName: '',
            lastName: '',
            userInfo: '',
            paymentInfo: '',
            paymentRate: 0
        }
    }

    console.log('ESTA ', bcrypt.compareSync(pass, tempHash))

    if (adminEmail.localeCompare(email.toLowerCase()) == 0) {
        res.redirect('administrator');
    } else {
        if (bcrypt.compareSync(pass, tempHash)) {
            userdb.create(user).then(r => {
                console.log('usuario creado ', r.email)
                ls('email', email.toLowerCase().trim());
                res.render('changepass');
            }).catch(e => { //if someone doesnt change password or a reset was made
                if (e.code == 400) {
                    console.log('ERROR ', e)
                    var messageData = {
                        style: 'display:block; color:#ff0000',
                        message: 'Error creating user'
                    }
                    res.render('index', messageData);
                }
            });
        } else {
            console.log('SI ENTRO AL ELSE DE LA WEA')
            userdb.getByEmail(email).then(foundUser => {
                console.log('user', foundUser)
                console.log('///////////EMAIL', email)
                console.log()
                if (bcrypt.compareSync(pass, foundUser.pass)) {
                    ls('email', email.toLowerCase().trim());
                    res.redirect(`https://www.wrike.com/oauth2/authorize/v4?client_id=${process.env.CLIENT_ID}&response_type=code`);
                } else {
                    var messageData = {
                        style: 'display:block; color:#ff0000',
                        message: 'Incorrect password'
                    }
                    res.render('index', messageData);
                }
            }).catch(e => {
                console.log("Error has ocurred ", e)
                var messageData = {
                    style: 'display:block; color:#ff0000',
                    message: 'Incorrect password'
                }
                res.render('index', messageData);
            });
        }
    }

    // userdb.create(user).then(r => {
    //     console.log('usuario creado ', r)
    //     ls('email', email);
    //     res.redirect(`https://www.wrike.com/oauth2/authorize/v4?client_id=${process.env.CLIENT_ID}&response_type=code`);
    // }).catch(e => {
    //     if (e.code == 400) {
    //         userdb.getByEmail(user.email).then(foundUser => {
    //             if (bcrypt.compareSync(pass, foundUser.pass)) {
    //                 ls('email', email);
    //                 res.redirect(`https://www.wrike.com/oauth2/authorize/v4?client_id=${process.env.CLIENT_ID}&response_type=code`);
    //             } else {
    //                 res.render('index', { title: 'Express' });
    //             }
    //         }).catch(e => console.log("Error has ocurred ", e));
    //         console.log("User created ", e.code)
    //     }
    // });
});

router.get('/accepted', function(req, res, next) {

    var code = req.query.code;

    var url = `https://www.wrike.com/oauth2/token?client_id=${process.env.CLIENT_ID}&client_secret=${process.env.CLIENT_SECRET}&grant_type=authorization_code&code=${code}`;


    return request.post(url).then((response) => {

        console.log(response)
        ls('authdata', response);
        userdb.getByEmail(ls.get('email')).then(r => {
            console.log(r)
            res.render('userdata', r)
        }).catch(e => console.log("Error has ocurred ", e));

    });
});


router.post('/dates', function(req, res, next) {

    var authData = JSON.parse(ls.get('authdata'))
    var authorization = 'Bearer ' + authData.access_token;
    var userInfo = req.body.userInfo;
    var paymentInfo = req.body.paymentInfo;
    var paymentRate = req.body.paymentRate;

    var url = `https://www.wrike.com/api/v4/contacts?me=true`

    const reqUserWrikeInfo = {
        headers: {
            'authorization': authorization
        },
        url: url
    };

    return request.get(reqUserWrikeInfo).then((response) => {

        var contact = JSON.parse(response)

        var firstName = contact.data[0].firstName;
        var lastName = contact.data[0].lastName;
        var id = contact.data[0].id;
        var email = ls.get('email');

        userdb.getByEmail(email).then(userFound => {

            userFound.contactInfo.firstName = firstName;
            userFound.contactInfo.lastName = lastName;
            userFound.contactInfo.id = id;
            userFound.contactInfo.userInfo = userInfo;
            userFound.contactInfo.paymentInfo = paymentInfo;
            userFound.contactInfo.paymentRate = paymentRate;
            ls('userInfo', userFound);
            ls('userId', userFound.contactInfo.id);
            // userdb.update(userFound).then(r => console.log("User created ", r)).catch(e => console.log("Error has ocurred ", e));

            userdb.delete(email).then(r => {
                userdb.create(userFound).then(r => {

                }).catch(e => { //if someone doesnt change password or a reset was made
                    if (e.code == 400) {
                        console.log('ERROR ', e)
                        var messageData = {
                            style: 'display:block; color:#ff0000',
                            message: 'Error creating user'
                        }
                        res.render('index', messageData);
                    }
                });

            }).catch(e => console.log("Error has ocurred ", e));


        }).catch(e => console.log("Error has ocurred ", e));

        var dates = {
            lastMonth: moment().format('MMMM YYYY'),
            thisMonth: moment().subtract(1, 'month').format('MMMM YYYY')
        }

        res.render('dates', { dates: dates })
    });

});


router.get('/testing', function(req, res, next) {

    const email = req.body.email;
    const pass = req.body.pass
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(pass, salt);

    var user = {
        email: email.toLowerCase(),
        pass: hash,
        contactInfo: {
            id: '',
            firstName: '',
            lastName: ' ',
            paymentInfo: '',
            paymentRate: 0
        }
    }

    userdb.create(user).then(r => {
        console.log('usuario creado ', r)

    }).catch(e => {
        if (e.code == 400) {
            userdb.getByEmail(user.email).then(foundUser => {
                if (bcrypt.compareSync(pass, foundUser.pass)) {
                    console.log('SI ES EL USUARIO')
                } else {
                    console.log('NO ES EL USUARIO')
                }
            }).catch(e => console.log("Error has ocurred ", e));
            console.log("User created ", e.code)
        } else {

        }
    });

    // userdb.create(user).then(r => console.log(r)).catch(e => console.log("Error has ocurred ", e));

    // userdb.getByEmail('angel.ojeda.castro@gmail.comm').then(r => console.log("User created ", r)).catch(e => console.log("Error has ocurred ", e));
    // userdb.update(user).then(r => console.log("User created ", r)).catch(e => console.log("Error has ocurred ", e));


    res.json({
        data: moment().format("YYYY-MM-DD[T]HH:mm:ss[Z]")
    })


});

router.post('/pass', function(req, res, next) {

    var authData = JSON.parse(ls.get('authdata'))
    var authorization = 'Bearer ' + authData.access_token;
    var dates;
    var contactInfoAndtimes = ls.get('userInfo').contactInfo;
    var categoriesMap = new Map();

    var totaltime = 0.0;


    if (req.body.selectedDate > 0) {
        dates = {
            start: moment().startOf('month').format("YYYY-MM-DD[T]HH:mm:ss"),
            end: moment().endOf('month').format("YYYY-MM-DD[T]HH:mm:ss")
        }
    } else {
        dates = {
            start: moment().startOf('month').subtract(1, 'month').format("YYYY-MM-DD[T]HH:mm:ss"),
            end: moment().subtract(1, 'month').endOf('month').format("YYYY-MM-DD[T]HH:mm:ss")
        }
    }

    // https://www.wrike.com/api/v4/contacts/KUAGA2M5/timelogs?createdDate={"start":"2019-07-01T06:00:00.000Z","finish":"2019-07-31T05:59:59.999Z"}
    //  https://www.wrike.com/api/v4/contacts/KUAGA2M5/timelogs?createdDate={"start":"2019-07-01T00:00:00Z","end":"2019-10-31T23:59:59Z"}
    // https://www.wrike.com/api/v4/contacts/KUGEYEFN/timelogs?trackedDate={"start":"2019-07-01T13:45:51Z","end":"2019-07-31T13:45:51Z"}

    var baseUrl = 'https://www.wrike.com/api/v4'

    var userId = ls.get('userId');
    // var url = `${baseUrl}/contacts/${userId}/timelogs?createdDate=` + JSON.stringify(dates)

    var url = `${baseUrl}/contacts/${userId}/timelogs?trackedDate=` + JSON.stringify(dates)

    console.log('////////////////////URL////////////////////////////', dates)



    const reqTimes = {
        headers: {
            'authorization': authorization
        },
        url: url
    };
    return request.get(reqTimes).then((response) => {
        var timelogs = JSON.parse(response).data;
        var projects = [];

        const reqCategories = {
            headers: {
                'authorization': authorization
            },
            url: `${baseUrl}/timelog_categories`
        };

        return request.get(reqCategories).then(categs => {
            var catList = JSON.parse(categs).data;

            allCategories = catList.map(category => {
                categoriesMap.set(category.id, category.name)
                return categoriesMap
            })

            Promise.all(allCategories).then(fullCategories => {

                mainProcess = timelogs.map(timelog => {
                    console.log('TIMELOG', timelog)
                    var taskId = timelog.taskId;
                    //var taskCategory = categories.get(timelog.categoryId)

                    url = `${baseUrl}/tasks/${taskId}`;
                    var reqParams = {
                        headers: reqTimes.headers,
                        url: url
                    }
                    return request.get(reqParams).then(resTaskStr => {
                        var resTask = JSON.parse(resTaskStr).data[0];
                        var project = [];

                        if (resTask.superParentIds.length > 0) {
                            project = projects.filter(p => resTask.superParentIds.filter(sp => p.projectId === sp)[0])[0];

                            if (project == undefined) {
                                project = {};
                                project.projectId = resTask.superParentIds[0];
                                project.categories = [];
                                projects.push(project)
                            }
                        } else if (resTask.parentIds.length > 0) {
                            project = projects.filter(p => resTask.parentIds.filter(sp => p.projectId === sp)[0])[0];
                            if (project == undefined) {
                                project = {};
                                project.projectId = resTask.parentIds[0];
                                project.categories = [];
                                projects.push(project)
                            }
                        }
                        var category = project.categories.filter(cat => cat.id === timelog.categoryId)[0]

                        if (category == undefined) {
                            category = {};
                            category.id = timelog.categoryId;
                            category.name = categoriesMap.get(timelog.categoryId);
                            category.tasks = [];
                            category.total = 0;
                            console.log('CATEGO ', timelog)
                            project.categories.push(category);
                        }
                        var task = {};
                        task = category.tasks.filter(ctask => ctask.id === resTask.id)[0];
                        if (task == undefined) {
                            task = {};
                            task.id = resTask.id;
                            task.title = resTask.title;
                            task.total = 0;
                            category.tasks.push(task);
                        }

                        task.total = task.total + timelog.hours;
                        category.total = category.total + timelog.hours;

                        if (!task.times) {
                            task.times = [];
                        }
                        var time = {};
                        time.description = timelog.comment;
                        time.time = timelog.hours.toFixed(2);
                        task.times.push(time);
                        return projects;
                    });
                })
                return Promise.all(mainProcess).then(v => {
                    var projectsIds = [];
                    projects.map(project => {
                        projectsIds.push(project.projectId);
                        var timeTotal = 0;
                        project.categories.map(cat => timeTotal += cat.total);
                        project.time = timeTotal.toFixed(2);
                        project.title = 'NA';
                        totaltime = totaltime + timeTotal

                    })
                    var ids = projectsIds.toString();
                    url = `${baseUrl}/folders/${ids}`;
                    var reqParams = {
                        headers: reqTimes.headers,
                        url: url
                    }

                    return request.get(reqParams).then(resProjectStr => {
                        var resProjects = JSON.parse(resProjectStr).data;
                        resProjects.map(resProject => {
                            var mProject = projects.filter(p => p.projectId === resProject.id)[0];
                            var pIdx = projects.indexOf(mProject);
                            mProject.title = resProject.title;
                            // var timeTotal = 0;
                            // mProject.tasks.map(task => timeTotal += task.total);
                            // mProject.time = timeTotal;
                            projects[pIdx] = mProject;
                        });
                        // console.log('projects', projects);
                        contactInfoAndtimes.times = projects
                        contactInfoAndtimes.totalTime = totaltime

                        // console.log('FINAL', contactInfoAndtimes)
                        // res.json(contactInfoAndtimes)
                        // res.render('invoice', contactInfoAndtimes)
                        ls('contactInfoAndtimes', contactInfoAndtimes);
                        res.redirect('/invoice')
                    });
                });

            })
        })
    });
});


module.exports = router;