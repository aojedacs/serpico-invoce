module.exports = (Schema) => {
    return new Schema({
        id: String,
        firstName: String,
        lastName: String,
        userInfo: String,
        paymentInfo: String,
        paymentRate: Number
    });
};