module.exports = (Schema) => {
    return new Schema({
        email: String,
        pass: String,
        contactInfo: {
            type: Schema.Types.Mixed,
            ref: 'Contact'
        }
    });
};