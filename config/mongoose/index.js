require('dotenv').config();
const mongoose = require('mongoose');
const { Schema } = mongoose;
mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true,useFindAndModify: false }).then(() => console.log('DB is Connected')).catch(err => console.error(err));

const User = mongoose.model('User', require('./models/User')(Schema));
const Contact = mongoose.model('Contact', require('./models/Contact')(Schema));

module.exports = { User, Contact };