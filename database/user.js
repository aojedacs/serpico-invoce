const db = require('../config/mongoose');

module.exports = {
    create: async data => {
        const { email, pass, contactInfo } = data;
        const item = new db.User({ email, pass, contactInfo });
        return await item.save();
    },
    update: async data => {
        const { email, pass, contactInfo } = data;
        return await db.User.findOneAndUpdate(email, { email, pass, contactInfo });
    },
    delete: async email => {
        return await db.User.findOneAndDelete({ email });
    },
    getByEmail: async email => {
        return await db.User.findOne({ email: email });
    },
    getAll: async() => {
        return await db.User.find();
    }
}