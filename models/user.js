const user = require('../database/user');

const model = {
    create: async data => {

        if (!data) throw { code: 400, msg: "Data is required" };
        if (!data.email) throw { code: 400, msg: "Email required" };
        if (!data.pass) throw { code: 400, msg: "data.pass Required" };
        // if (!data.contactInfo) throw { code: 400, msg: "Display name Required" };

        const userExists = await user.getByEmail(data.email);
        if (userExists) throw { code: 400, msg: "The user already exists" };

        return await user.create(data);
    },
    update: async data => {
        if (!data) throw { code: 400, msg: "Data is required" };

        return await user.update(data);
    },
    delete: async data => {
        if (!data) throw { code: 400, msg: "Params is empty" };
        return await user.delete(data);
    },
    getByEmail: async email => {
        return await user.getByEmail(email);
    },
    getAll: async() => {
        return await user.getAll();
    },
}

module.exports = model;